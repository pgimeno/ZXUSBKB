# USBZX keyboard

This project turns an empty ZX81 or ZX Spectrum with a working membrane into a USB keyboard.

## WARNING! WARNING!

The PCBs are not in a usable state at this point. The repo will be updated when that happens.

## Features

- Two modes: standard keyboard protocol (with 6KRO) and recreated ZX Spectrum protocol (with NKRO). Note also that the ghosting is inherent to the membrane keyboard and can't be avoided.
- Kicad PCBs for a robust placement inside the computers.
- The same MCU and software can be used for either a ZX81 or a Spectrum case. Both are able to drive either a ZX81 or a Spectrum emulator (the difference is in what the Symbol Shift key, in a Spectrum case, or the Dot key, in a ZX81 case, produces).

## Tools needed

For the hardware, you need:

- A Pro Micro (a circuit board based on the ATmega32U4 USB microcontroller); it doesn't matter whether it's the 3.3V or the 5V version.
- Keyboard membrane connectors
- PCB pin rows and female connectors for these
- A MicroUSB cable
- The PCB(s) you plan to use
  - If you plan to make changes to the PCB, you'll also need Kicad; otherwise you can use the pre-built Gerber files in the releases page to order them
- Jumpers to select the mode

As for the software, to compile it yourself, you need:

- gcc-avr
- avr-libc (normally pulled by gcc-avr)
- binutils-avr
- avrdude
- GNU make

You can then build the software by just typing `make` after changing to the directory where the software is.

To upload it to the MCU, first you need to reset it. You can do that by connecting the third pin of each side, counting from the USB connector. Look up the pinout of a Pro Micro if in doubt - you have to bridge GND with RST. It will be listening for about 8-10 seconds and then the built-in program will automatically start, so you have to send the program within those 8-10 seconds. To send it, use `make upload`.

## Modes of operation

There are three jumpers that can be used to select one of seven modes (you can connect switches instead of jumpers, of course).

- The top jumper selects Recreated mode.
- The middle jumper selects Alternative mode.
- The bottom jumper selects ZX81 mode.

When in Recreated mode, Alternative mode selects strict protocol, and the ZX81 mode jumper does nothing.

When in Standard (not Recreated) Spectrum mode, Alternative mode selects whether SYMBOL SHIFT will generate Left Ctrl (no jumper) or Right Shift (jumper placed). In Standard ZX81 mode (bottom jumper placed), Alternative mode changes whether the SHIFT key will generate Left Shift (no jumper) or Left Ctrl (jumper placed).

There is a Reset jumper that you don't need to install; it might be useful if you plan to re-flash the MCU software.

## Notes on Standard mode

All letters and numbers are sent as themselves. The ZX81/Spectrum ENTER key is sent as the USB Return key.

When pressing CAPS SHIFT in a Spectrum, the Left Shift key will be sent. In a ZX81, the SHIFT key will send Left Shift in normal mode, or Left Ctrl in Alternative mode.

When pressing SYMBOL SHIFT in a Spectrum, with the Alternative mode jumper not set, the Left Ctrl key will be sent. This is compatible with MiSTer and FUSE, but some emulators may have other key assignments that cause incompatibilities; for example MAME's default setup expects a right shift key for SYMBOL SHIFT, and the Alternative mode jumper will switch to that. In ZX81 mode, a period will be sent instead of the Left Ctrl or Right Shift key.

The USB protocol used supports a maximum of 6 keypresses at the same time (so called 6KRO); this limits the maximum number of keys that can be used in Standard mode.

## Notes on Recreated mode

The Recreated mode works in such way that each keypress is converted to a certain key press and release, and each key release is converted to a *different* press and release. So, it basically works by converting presses and releases to events, instead of filling in a buffer with the keys that are currently pressed as standard mode does. Doing it this way, NKRO (any number of pressed keys at once) is possible.

Since there are twice as many events as there are keys, the Recreated protocol needs some keys to be pressed together with shift. For the keys that need shift, the original Recreated ZX Spectrum keyboard sends a sequence of pressing shift, pressing the relevant key, releasing the key, and releasing shift, and does that for every ZX key event, be it a press or a release. We call this Strict protocol.

The MCU software supports by default a Relaxed protocol, which presses and holds shift while sending all keys that need it pressed. This works fine in the MiSTer cores, because they track the status of the shift key in order to know how to interpret the other key. This saves USB transmissions and thus reduces lag. But FUSE, and possibly other emulators with Recreated support, only works well if each key that needs shift is prefixed with an explicit press of the shift key; holding shift and then pressing more than one key will miss the fact that the second key is shifted. As a consequence, the Relaxed protocol mode of this software is not fully compatible with those emulators, and you need to enable the Strict protocol by placing the middle jumper.

## USB Library

Thanks to PJRC for the USB library used in this project.
