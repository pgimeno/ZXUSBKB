/*
 * USBZX - Turn ZX81 or ZX Spectrum into a USB keyboard
 *
 * (C) Copyright 2023 Pedro Gimeno Fortea
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>

#include "usb_keyboard.h"

// Physical layout:
// PD4-PD0: columns
// PB3,PF7,PF5,PF4,PF6,PB1,PB2,PB6 (in that order): rows
// We will output to rows and read from columns.

static uint8_t kbdstate[8]; // State of each key, shared between modes
static uint8_t oldstate[8]; // used to detect press/release events in Recr mode
static uint8_t sendcnt; // number of Recr events to send in a single packet

static uint8_t qlen; // number of keys in keyboard_keys in Std mode

// Wait for output to settle, to be able to read a meaningful input
static void insert_wait()
{
  for (uint8_t i = 160; i; --i)
    __asm__(";");
}


static void scankbd(void)
{
  // Switching all ports to high-Z except the one being probed voids the need
  // for diodes in row lines (helped by the pull-ups in the columns)

  DDRB |= (1<<3);  // PB3 output
  insert_wait(); // time to stabilize
  kbdstate[0] = PIND & 0x1F;
  DDRB &= ~(1<<3); // PB3 input

  DDRF |= (1<<6);  // PF6 output
  insert_wait();
  kbdstate[1] = PIND & 0x1F;
  DDRF &= ~(1<<6); // PF6 input

  DDRF |= (1<<5);  // PF5 output
  insert_wait();
  kbdstate[2] = PIND & 0x1F;
  DDRF &= ~(1<<5); // PF5 input

  DDRF |= (1<<4);  // PF4 output
  insert_wait();
  kbdstate[3] = PIND & 0x1F;
  DDRF &= ~(1<<4); // PF4 input

  DDRF |= (1<<7);  // PF7 output
  insert_wait();
  kbdstate[4] = PIND & 0x1F;
  DDRF &= ~(1<<7); // PF7 input

  DDRB |= (1<<1);  // PB1 output
  insert_wait();
  kbdstate[5] = PIND & 0x1F;
  DDRB &= ~(1<<1); // PB1 input

  DDRB |= (1<<2);  // PB2 output
  insert_wait();
  kbdstate[6] = PIND & 0x1F;
  DDRB &= ~(1<<2); // PB2 input

  DDRB |= (1<<6);  // PB6 output
  insert_wait();
  kbdstate[7] = PIND & 0x1F;
  DDRB &= ~(1<<6); // PB6 input
}


typedef struct {
  uint8_t scanMake;
  bool shiftMake;
  uint8_t scanBreak;
  bool shiftBreak;
} t_keydef;

// Recreated codes
static const t_keydef rkbcodes[40] = {
  {KEY_8, false, KEY_9, false}, {KEY_COMMA, true, KEY_PERIOD, true},
  {KEY_MINUS, false, KEY_EQUAL, false},
  {KEY_LEFT_BRACE, false, KEY_RIGHT_BRACE, false},
  {KEY_SEMICOLON, false, KEY_SEMICOLON, true},

  {KEY_O, true, KEY_P, true}, {KEY_Q, true, KEY_R, true},
  {KEY_S, true, KEY_T, true}, {KEY_U, true, KEY_V, true},
  {KEY_W, true, KEY_X, true},

  {KEY_U, false, KEY_V, false}, {KEY_W, false, KEY_X, false},
  {KEY_Y, false, KEY_Z, false}, {KEY_A, true, KEY_B, true},
  {KEY_C, true, KEY_D, true},

  {KEY_A, false, KEY_B, false}, {KEY_C, false, KEY_D, false},
  {KEY_E, false, KEY_F, false}, {KEY_G, false, KEY_H, false},
  {KEY_I, false, KEY_J, false},

  {KEY_S, false, KEY_T, false}, {KEY_Q, false, KEY_R, false},
  {KEY_O, false, KEY_P, false}, {KEY_M, false, KEY_N, false},
  {KEY_K, false, KEY_L, false},

  {KEY_M, true, KEY_N, true}, {KEY_K, true, KEY_L, true},
  {KEY_I, true, KEY_J, true}, {KEY_G, true, KEY_H, true},
  {KEY_E, true, KEY_F, true},

  {KEY_6, false, KEY_7, false}, {KEY_4, false, KEY_5, false},
  {KEY_2, false, KEY_3, false}, {KEY_0, false, KEY_1, false},
  {KEY_Y, true, KEY_Z, true},

  {KEY_5, true, KEY_6, true}, {KEY_1, true, KEY_4, true},
  {KEY_LEFT_BRACE, true, KEY_RIGHT_BRACE, true},
  {KEY_SLASH, false, KEY_SLASH, true}, {KEY_COMMA, false, KEY_PERIOD, false},
};

// Standard codes
static const uint8_t skbcodes[40] = {
  0,     KEY_Z, KEY_X, KEY_C, KEY_V,
  KEY_A, KEY_S, KEY_D, KEY_F, KEY_G,
  KEY_Q, KEY_W, KEY_E, KEY_R, KEY_T,
  KEY_1, KEY_2, KEY_3, KEY_4, KEY_5,
  KEY_0, KEY_9, KEY_8, KEY_7, KEY_6,
  KEY_P, KEY_O, KEY_I, KEY_U, KEY_Y,
  KEY_ENTER, KEY_L, KEY_K, KEY_J, KEY_H,
  KEY_SPACE, KEY_PERIOD, KEY_M, KEY_N, KEY_B,
};

// Adds the key to the buffer and marks it as processed
// Returns whether buffer is full
static inline bool addKey(uint8_t scancode, uint8_t row, uint8_t mask)
{
  // Store the key in the buffer
  keyboard_keys[sendcnt++] = scancode;
  // Set the correct bit of oldstate[row] = corresponding bit in kbdstate[row]
  oldstate[row] = ((oldstate[row] ^ kbdstate[row]) & mask) ^ oldstate[row];
  // Return whether the buffer is full
  return sendcnt == 6;
}


int main()
{
  // set for 16 MHz clock
  CLKPR = 0x80;
  CLKPR = 0;

  DDRB  = 0x01; // PB0 is RXLED so set it to output mode, rest in input mode
  PORTB = 0x30; // Pull-ups in PB4/PB5, led ON (0)

  DDRC  = 0x00; // All pins of port C in input mode
  PORTC = 0x40; // Pull-up in PC6 which is one of the mode selectors

  DDRD  = 0x20; // PD5 is TXLED so set it to output mode, rest in input mode
  PORTD = 0xBF; // Pull-ups for cols and PD7 (mode2 selector), led OFF (1)

  DDRE  = 0x00; // All pins of port E in input mode
  PORTE = 0x40; // Pull-up on PE6

  DDRF  = 0x00; // All pins of port F in input mode
  PORTF = 0x00; // No pull-ups

  usb_init();
  while (!usb_configured()) /* wait */ ;

#define RECRMODE ((PINC & 0x40) == 0)
#define ALTMODE ((PIND & 0x80) == 0)
#define ZX81MODE ((PINE & 0x40) == 0)

  // set RXLED OFF to indicate end of initialization
  PORTB |= 0x01;

  // Initialize to opposite of initial mode, to force a change
  bool recrmode = !RECRMODE;

  do
  {
    // Assign new key states
    scankbd();

    // Cache the modes
    bool zx81mode = ZX81MODE;
    bool altmode = ALTMODE;
    bool oldrecrmode = recrmode;
    recrmode = RECRMODE;

    uint8_t keyindex;

    if (oldrecrmode != recrmode)
    {
      if (!recrmode)
      {
        // Switch from Recreated to Standard mode
        qlen = 0;
        keyboard_modifier_keys = 0;
        keyboard_keys[0] = 0;
        keyboard_keys[1] = 0;
        keyboard_keys[2] = 0;
        keyboard_keys[3] = 0;
        keyboard_keys[4] = 0;
        keyboard_keys[5] = 0;
        PORTD |= 0x20; // Switch off TX LED
      }
      else
      {
        // Switch from Standard to Recreated mode
        // Set all oldstate to not pressed
        oldstate[0] = 0b00011111;
        oldstate[1] = 0b00011111;
        oldstate[2] = 0b00011111;
        oldstate[3] = 0b00011111;
        oldstate[4] = 0b00011111;
        oldstate[5] = 0b00011111;
        oldstate[6] = 0b00011111;
        oldstate[7] = 0b00011111;
        PORTD &= ~0x20; // Switch on TX LED
      }
    }

    if (recrmode)
      goto process_std_mode;

    // Recreated mode

    uint8_t oldsendcnt = sendcnt;
    sendcnt = 0;
    keyboard_modifier_keys = 0;
    keyboard_keys[0] = 0;
    keyboard_keys[1] = 0;
    keyboard_keys[2] = 0;
    keyboard_keys[3] = 0;
    keyboard_keys[4] = 0;
    keyboard_keys[5] = 0;

    // Loop over unshifted keys first
    keyindex = 0;
    for (uint8_t row = 0; row < 8; ++row)
    {
      uint8_t diff = kbdstate[row] ^ oldstate[row];
      for (uint8_t mask = 1; mask != 0x20; mask <<= 1, ++keyindex)
      {
        if (!rkbcodes[keyindex].shiftMake) // unshifted
        {
          if (diff & mask & ~kbdstate[row]) // key just pressed
            if (addKey(rkbcodes[keyindex].scanMake, row, mask)) // add the key
              goto send; // USB key buffer full
        }
        if (!rkbcodes[keyindex].shiftBreak) // unshifted
        {
          if (diff & mask & kbdstate[row]) // key just released
            if (addKey(rkbcodes[keyindex].scanBreak, row, mask)) // add the key
              goto send; // USB key buffer full
        }
      }
    }

    // We can't send shifted keys in the same packet as the unshifted ones,
    // because the shift affects all keys sent, so we send them in advance.
    if (sendcnt) goto send; // send all unshifted keys if there are any

    // Loop over shifted keys now
    keyindex = 0;
    for (uint8_t row = 0; row < 8; ++row)
    {
      uint8_t diff = kbdstate[row] ^ oldstate[row];
      for (uint8_t mask = 1; mask != 0x20; mask <<= 1, ++keyindex)
      {
        if (rkbcodes[keyindex].shiftMake) // shifted
        {
          if (diff & mask & ~kbdstate[row]) // key just pressed
          {
            if (altmode)
            {
              // Strict Recreated protocol - send the keys one by one
              keyboard_modifier_keys = KEY_LEFT_SHIFT;
              addKey(rkbcodes[keyindex].scanMake, row, mask);
              // Trick our code into sending an event,
              // without sending anything in the next iteration.
              oldsendcnt = 1;
              sendcnt = 0;
              goto send;
            }
            // Relaxed protocol - combine several keys with one shift event
            if (addKey(rkbcodes[keyindex].scanMake, row, mask)) // add the key
              goto shiftedsend; // USB key buffer full
          }
        }
        if (rkbcodes[keyindex].shiftBreak) // shifted
        {
          if (diff & mask & kbdstate[row]) // key just released
          {
            if (altmode)
            {
              // Strict Recreated protocol - send the keys one by one
              keyboard_modifier_keys = KEY_LEFT_SHIFT;
              addKey(rkbcodes[keyindex].scanBreak, row, mask);
              // Trick our code into sending an event,
              // without sending anything in the next iteration.
              oldsendcnt = 1;
              sendcnt = 0;
              goto send;
            }
            // Relaxed protocol - combine several keys with one shift event
            if (addKey(rkbcodes[keyindex].scanBreak, row, mask)) // add the key
              goto shiftedsend; // USB key buffer full
          }
        }
      }
    }


shiftedsend:
    if (sendcnt) keyboard_modifier_keys = KEY_LEFT_SHIFT;

send:
    // Send keys if there are any or there were any in the previous iteration
    if (sendcnt || oldsendcnt)
      usb_keyboard_send();

    if (keyboard_modifier_keys)
    {
      // If shift was pressed, send another packet with just shift, to release
      // all other keys before shift. This is because shift must be the last
      // key to be released, to prevent a race with pressing the unshifted key
      // instead of the shifted one.
      keyboard_keys[0] = 0;
      keyboard_keys[1] = 0;
      keyboard_keys[2] = 0;
      keyboard_keys[3] = 0;
      keyboard_keys[4] = 0;
      keyboard_keys[5] = 0;
      usb_keyboard_send();

      if (altmode)
      {
        // Strict protocol requires a separate event for releasing shift
        keyboard_modifier_keys = 0;
        usb_keyboard_send();
      }
    }
    continue;

process_std_mode: ;
    // Remove from the buffer the keys that are not pressed
    keyindex = 0;
    for (uint8_t row = 0; row < 8; ++row)
    {
      uint8_t keyrow = kbdstate[row];

      for (uint8_t mask = 1; mask != 0x20; mask <<= 1, ++keyindex)
      {
        // Skip shift keys (keyindex 0 and in Spectrum mode, keyindex 36)
        // and check if the key is released
        if (keyindex && (keyindex != 36 || zx81mode) && mask & keyrow)
        {
          // Search for the key and remove it if present
          uint8_t keyscan = skbcodes[keyindex];
          for (uint8_t i = qlen; i; )
          {
            if (keyboard_keys[--i] == keyscan)
            {
              // Key present - remove it from the buffer
              keyboard_keys[i] = keyboard_keys[--qlen];
              keyboard_keys[qlen] = 0;
            }
          }
        }
      }
    }

    // Add keys to the buffer (modifier key state is always sent, in
    // keyboard_modifier_keys)
    keyindex = 0;
    for (uint8_t row = 0; row < 8; ++row)
    {
      uint8_t keyrow = kbdstate[row];

      for (uint8_t mask = 1; mask != 0x20; mask <<= 1, ++keyindex)
      {
        if (!keyindex) // Caps Shift
          keyboard_modifier_keys = mask & keyrow ? 0 :
            zx81mode && altmode ? KEY_LEFT_CTRL : KEY_LEFT_SHIFT;
        else if (keyindex == 36 && !zx81mode) // Symbol Shift
        {
          // In normal mode, LCtrl is the Symbol Shift key. In Alternate mode,
          // it's RShift.
          if (mask & ~keyrow) keyboard_modifier_keys |=
            altmode ? KEY_RIGHT_SHIFT : KEY_LEFT_CTRL;
        }
        else if (mask & ~keyrow && qlen < 6)
        {
          // Add the pressed keys that are not in the buffer yet
          uint8_t keyscan = skbcodes[keyindex];
          for (uint8_t i = qlen; i; )
          {
            if (keyboard_keys[--i] == keyscan)
            {
              // Already in buffer - skip
              keyscan = 0;
              break;
            }
          }
          if (keyscan)
            keyboard_keys[qlen++] = keyscan;
        }
      }
    }

    // Done, send the buffer
    usb_keyboard_send();

  }
  while (1);
  __builtin_unreachable();
}
