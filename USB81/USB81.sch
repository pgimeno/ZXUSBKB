EESchema Schematic File Version 4
LIBS:USB81-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ZX81 as USB Keyboard"
Date "2023-05-21"
Rev "1.0"
Comp "© Copyright 2023 Pedro Gimeno Fortea"
Comment1 "Use a ZX81 as a USB keyboard"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 645E8267
P 8650 3100
F 0 "J2" V 8614 2612 50  0000 R CNN
F 1 "COLS" V 8523 2612 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x08_P2.54mm_Vertical" H 8650 3100 50  0001 C CNN
F 3 "~" H 8650 3100 50  0001 C CNN
	1    8650 3100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5700 2750 2750 2750
Wire Wire Line
	5700 2350 2550 2350
Wire Wire Line
	2550 2350 2550 2900
Wire Wire Line
	2750 2750 2750 2900
Wire Wire Line
	2850 2850 2850 2900
Wire Wire Line
	5700 2450 2650 2450
$Comp
L Connector_Generic:Conn_01x05 J1
U 1 1 645E7C04
P 2650 3100
F 0 "J1" V 2522 3380 50  0000 L CNN
F 1 "ROWS" V 2613 3380 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 2650 3100 50  0001 C CNN
F 3 "~" H 2650 3100 50  0001 C CNN
	1    2650 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 2450 2650 2900
Wire Wire Line
	2850 2850 5700 2850
Wire Wire Line
	2450 2900 2450 2650
Wire Wire Line
	2450 2650 5450 2650
Wire Wire Line
	5450 2650 5450 2950
Wire Wire Line
	5450 2950 5700 2950
$Comp
L Connector_Generic:Conn_02x12_Counter_Clockwise MCU1
U 1 1 645FD121
P 5900 2850
F 0 "MCU1" H 5950 3641 50  0000 C CNN
F 1 "Pro Micro" H 5950 3512 100 0000 C CNN
F 2 "Package_DIP:DIP-24_W15.24mm" H 5900 2850 50  0001 C CNN
F 3 "~" H 5900 2850 50  0001 C CNN
	1    5900 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 3300 7650 3300
Wire Wire Line
	7650 3300 7650 2750
Wire Wire Line
	7650 2750 6200 2750
Wire Wire Line
	8450 3300 8450 3350
Wire Wire Line
	8450 3350 7550 3350
Wire Wire Line
	7550 3350 7550 2850
Wire Wire Line
	7550 2850 6200 2850
Wire Wire Line
	7450 3400 7450 2950
Wire Wire Line
	7450 2950 6200 2950
Wire Wire Line
	7350 3450 7350 3050
Wire Wire Line
	7350 3050 6200 3050
Wire Wire Line
	7250 3500 7250 3150
Wire Wire Line
	7250 3150 6200 3150
Wire Wire Line
	8850 3300 8850 3550
Wire Wire Line
	8850 3550 7150 3550
Wire Wire Line
	7150 3550 7150 3250
Wire Wire Line
	7150 3250 6200 3250
Wire Wire Line
	8950 3300 8950 3600
Wire Wire Line
	8950 3600 7050 3600
Wire Wire Line
	7050 3600 7050 3350
Wire Wire Line
	7050 3350 6200 3350
Wire Wire Line
	9050 3300 9050 3650
Wire Wire Line
	9050 3650 6950 3650
Wire Wire Line
	6950 3650 6950 3450
Wire Wire Line
	6950 3450 6200 3450
Wire Wire Line
	4400 3050 4400 2550
Wire Wire Line
	4400 2550 5700 2550
Wire Wire Line
	5700 3150 4950 3150
Wire Wire Line
	4450 3050 4400 3050
Wire Wire Line
	4950 3050 5700 3050
Wire Wire Line
	8550 3450 7350 3450
Wire Wire Line
	8550 3300 8550 3450
Wire Wire Line
	8750 3500 7250 3500
Wire Wire Line
	8750 3300 8750 3500
Wire Wire Line
	8650 3300 8650 3400
Wire Wire Line
	8650 3400 7450 3400
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J3
U 1 1 646A8E4C
P 4650 3050
F 0 "J3" H 4700 3267 50  0000 C CNN
F 1 "MODE_SEL" H 4700 3176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x02_P2.54mm_Vertical" H 4650 3050 50  0001 C CNN
F 3 "~" H 4650 3050 50  0001 C CNN
	1    4650 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3150 4400 3150
Wire Wire Line
	4400 3150 4400 3050
Connection ~ 4400 3050
$EndSCHEMATC
